# It's useful to setup the current version of our code in the build system
# using a `semver` style
set (VERSION_MAJOR 1)
set (VERSION_MINOR 0)
set (VERSION_PATCH 0)

# Send the variables (version number) to source code header
configure_file (
  "${PROJECT_SOURCE_DIR}/src/version.h.in"
  "${PROJECT_BINARY_DIR}/src/version.h"
)

set(PROJECT_SOURCES
  main.cpp
)

add_executable(
    ${PROJECT_NAME}
    ${PROJECT_SOURCES}
)