/* 
 * File:   main.cpp
 * Author: Piotr Hoppe
 * 
 * Created on 29 marca 2019, 21:43
 */

#include "version.h"
#include <iostream>
#include <sstream>

std::string getVersionString(void)
{
    std::ostringstream versionString;

    versionString << VERSION_MAJOR << "." << VERSION_MINOR << "." << VERSION_PATCH;

    return versionString.str();
}

int main(int argc, char *argv[])
{
    std::cout << "Version: " << getVersionString() << std::endl;
}